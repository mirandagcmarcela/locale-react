import React from 'react';
import Header from './Header';
import { FormattedMessage } from 'react-intl';

const Explore = () => {
	return (
		<div>
			<Header />
			<div className="main">
				<h1 className="titulo">
					<FormattedMessage 
						id="explore.title"
						defaultMessage="Explore"
					/>
				</h1>
			</div>
		</div>
	);
}
 
export default Explore;