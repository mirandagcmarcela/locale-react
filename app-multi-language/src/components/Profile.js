import React from 'react';
import Header from './Header';
import { FormattedMessage } from 'react-intl';

const Profile = () => {
	return (
		<div>
			<Header />

			<div className="main">
				<h1 className="titulo">
					<FormattedMessage 
						id="profile.title"
						defaultMessage="Explore"
					/>
				</h1>
			</div>
		</div>
	);
}
 
export default Profile;