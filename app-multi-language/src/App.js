import React from 'react';
import Header from './components/Header';
import { FormattedMessage, FormattedDate, FormattedNumber, FormattedTime, formatNumber } from 'react-intl';

import './styles.css';

const App = () => {

	return (
		<div>
			<Header />
			<div className="main">
				<h1 className="titulo">
					<FormattedMessage 
						id="app.welcome" 
						defaultMessage="Welcome"
						values={{name:"DEV31"}}
					/>
				</h1>
				<p className="moneda1">
					<FormattedNumber 
						value={19} 
						style="currency" 
						currency="EUR" 
					/>
				</p>
				<p className="moneda2">
					<FormattedNumber 
						value={109} 
						style="currency" 
						currency="USD" 
					/>
				</p>
				<p className="subtitulo">
					<FormattedDate 
						value={Date.now()}
						year="numeric"
						month="long"
						day="numeric"
						weekday="long"
					/>
				</p>
				<p className="tiempo">
					<FormattedTime 
						value={Date.now()}
						style="narrow"
					/>
				</p>
			</div>
		</div>
	);
}
 
export default App;
