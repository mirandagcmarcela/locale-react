import React, { useState } from 'react';
import englishMessages from '../languages/en-US.json';
import spanishMessages from '../languages/es-MX.json';
import { IntlProvider } from 'react-intl';

//global state = context
const langContext = React.createContext();

//provider for close all the application
const LangProvider = ({children}) => {
	const [messages, establishMessages] = useState(englishMessages);
	const [locale, establishLocale] = useState('en-US');

	const establishLanguage = (language) => {
		switch(language){
			case 'es-MX':
				establishMessages(spanishMessages);
				establishLocale('es-MX');
				break;
			case 'en-US':
				establishMessages(englishMessages);
				establishLocale('en-US');
				break;
			default:
				establishMessages(englishMessages);
				establishLocale('en-US');
		}
	}

	return (
		<langContext.Provider value={{establishLanguage: establishLanguage}}>
			<IntlProvider locale={locale} messages={messages} defaultLocale='en-US'>
				{children}
			</IntlProvider>
		</langContext.Provider>
	);
}
export { LangProvider, langContext };
